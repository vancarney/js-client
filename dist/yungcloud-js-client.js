var RikkiTikki, self,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

if (((typeof module !== "undefined" && module !== null ? module.exports : void 0) != null)) {
  RikkiTikki = require('riiki-tikki-client');
}

self = typeof exports !== "undefined" && exports !== null ? exports : window;

self.Yungcloud = RikkiTikki.createScope('yungcloud');

self.Yungcloud.Object.prototype.idAttribute = 'id';

Yungcloud.Auth.prototype.persist = true;

Yungcloud.Login.prototype.className = 'User';

Yungcloud.API_VERSION = 'v1';

Yungcloud.track = (function(superClass) {
  var addPlaylist, removePlaylist;

  extend(track, superClass);

  function track() {
    return track.__super__.constructor.apply(this, arguments);
  }

  track.prototype.defaults = {
    title: null,
    buy: null,
    account_id: null,
    description: null,
    tag: null,
    size: 0,
    "public": 1,
    download: 0,
    license: "all rights reserved"
  };

  track.prototype.validate = function(o) {
    var ref, ref1, ref2;
    _.extend(this.attributes, o);
    if (!((ref = o.title) != null ? ref.length : void 0)) {
      return "track title required";
    }
    if (!((ref1 = o.tag) != null ? ref1.length : void 0)) {
      return "tags required";
    }
    if ((ref2 = o.buy) != null ? ref2.length : void 0) {
      if (!o.buy.match(/^(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/ig)) {
        return "buy link must be a valid URL";
      }
    }
  };

  track.prototype.save = function() {
    return Backbone.Model.prototype.save.apply(this, arguments);
  };

  addPlaylist = function(playlistOrId, options) {
    var _opts, o, playlistId;
    if (options == null) {
      options = {};
    }
    if (!(playlistId = Util.getID(playlistOrId))) {
      if (typeof options.error === "function") {
        options.error('invalid playlistId');
      }
      return false;
    }
    _opts = {
      type: 'put',
      validate: false,
      url: (this.url().replace('?', '')) + "/playlists/rel/" + playlistId,
      success: (function(_this) {
        return function(m, r, o) {
          return _this.fetch(options.success != null ? {
            success: options.success
          } : {});
        };
      })(this)
    };
    o = {
      track_id: this.get('id'),
      playlist_id: playlistId
    };
    return this.clone().clear({
      silent: true
    }).save(o, _.extend(options, _opts));
  };

  removePlaylist = function(playlistOrId, options) {
    var _opts, o, playlistId;
    if (options == null) {
      options = {};
    }
    if (!(playlistId = Util.getID(playlistOrId))) {
      if (typeof options.error === "function") {
        options.error('invalid playlistId');
      }
      return false;
    }
    _opts = {
      url: (this.url().replace('?', '')) + "/playlists/rel/" + playlistId,
      success: (function(_this) {
        return function(m, r, o) {
          return _this.fetch(options.success != null ? {
            success: options.success
          } : {});
        };
      })(this)
    };
    o = {
      track_id: this.get('id'),
      playlist_id: playlistId
    };
    return this.clone().clear({
      silent: true
    }).destroy(_.extend(options, _opts));
  };

  return track;

})(Yungcloud.Model);

Yungcloud.Register = (function(superClass) {
  extend(Register, superClass);

  function Register() {
    return Register.__super__.constructor.apply(this, arguments);
  }

  Register.prototype.className = 'Account';

  Register.prototype.defaults = {
    username: "",
    password: "",
    email: ""
  };

  Register.prototype.validate = function(o) {
    var ref, ref1, ref2;
    _.extend(this.attributes, o);
    if (!((ref = o.username) != null ? ref.length : void 0)) {
      return "username required";
    }
    if (!((ref1 = o.email) != null ? ref1.length : void 0)) {
      return "email required";
    }
    if (!((ref2 = o.password) != null ? ref2.length : void 0)) {
      return "password required";
    }
  };

  return Register;

})(Yungcloud.Model);

self.Yungcloud.tracks = (function(superClass) {
  extend(tracks, superClass);

  function tracks() {
    return tracks.__super__.constructor.apply(this, arguments);
  }

  tracks.prototype.model = self.Yungcloud.track;

  return tracks;

})(self.Yungcloud.Collection);

self.Yungcloud.profile = (function(superClass) {
  extend(profile, superClass);

  function profile() {
    return profile.__super__.constructor.apply(this, arguments);
  }

  profile.prototype.defaults = {
    firstName: "",
    lastName: "",
    email: "",
    city: "",
    country: "",
    postal: "",
    image: "",
    cover: ""
  };

  return profile;

})(self.Yungcloud.Model);

self.Yungcloud.Settings = (function(superClass) {
  extend(Settings, superClass);

  function Settings() {
    return Settings.__super__.constructor.apply(this, arguments);
  }

  Settings.prototype._auth = Yungcloud.Auth.getInstance();

  Settings.prototype.defaults = {
    account_id: null,
    notify_likes: true,
    notify_comments: true,
    notify_chats: true,
    notify_friends: true,
    email_comments: true,
    email_likes: true,
    email_friends: true
  };

  Settings.prototype.validate = function(o) {
    console.log(o);
  };

  Settings.prototype.fetch = function() {
    if (!(this._auth.isAuthenticated() && (this.get('account_id') != null))) {
      return false;
    }
    return Settings.__super__.fetch.apply(this, arguments);
  };

  Settings.prototype.save = function(model, options) {
    if (options == null) {
      options = {};
    }
    if (!(this._auth.isAuthenticated() && (this.get('account_id') != null))) {
      return false;
    }
    options.type = 'put';
    return Settings.__super__.save.call(this, model, options);
  };

  Settings.prototype.destroy = function() {
    return false;
  };

  Settings.prototype.initialize = function() {
    if (this._auth.isAuthenticated()) {
      this.set({
        account_id: this._auth.getUser().id
      }, {
        silent: true
      });
    }
    this._auth.on('authenticated', (function(_this) {
      return function() {
        _this.set({
          account_id: _this._auth.getUser().id
        });
        return _this.fetch();
      };
    })(this));
    return this._auth.on('deauthenticated', (function(_this) {
      return function() {
        return _this.clear();
      };
    })(this));
  };

  return Settings;

})(self.Yungcloud.Model);

self.Yungcloud.profiles = (function(superClass) {
  extend(profiles, superClass);

  function profiles() {
    return profiles.__super__.constructor.apply(this, arguments);
  }

  profiles.prototype.model = self.Yungcloud.profile;

  return profiles;

})(self.Yungcloud.Collection);

self.Yungcloud.Account = (function(superClass) {
  extend(Account, superClass);

  function Account() {
    return Account.__super__.constructor.apply(this, arguments);
  }

  Account.prototype._auth = Yungcloud.Auth.getInstance();

  Account.prototype.sync = function() {
    if (!(this._auth.isAuthenticated() && (this.get('id') != null))) {
      throw 'cannot operate on Account object without valid user';
    }
    return Account.__super__.sync.apply(this, arguments);
  };

  Account.prototype.resetPassword = function(m, opts) {
    return (new Yungcloud.users).reset.apply(this, arguments);
  };

  Account.prototype.getSettings = function(options) {
    var id;
    if ((id = this.get('id')) == null) {
      return;
    }
    options.url = (this.url().split('?')[0]) + "/settings";
    return (new Yungcloud.Settings).fetch(options);
  };

  Account.prototype.url = function() {
    var filter, url;
    url = (Yungcloud.getAPIUrl()) + "/accounts/" + this.id;
    filter = JSON.stringify({
      include: ["avatars", "banners"]
    });
    return url + "?filter=" + filter;
  };

  Account.prototype.updateSettings = function(value, options) {
    var id;
    if ((id = this.get('id')) == null) {
      return;
    }
    if (value.hasOwnProperty('id')) {
      delete value.id;
    }
    options.type = 'put';
    options.url = (this.url()) + "/" + id + "/settings";
    return (new Yungcloud.Settings({
      id: id
    })).save(value, options);
  };

  Account.prototype.refresh = function(options) {
    if (options == null) {
      options = {};
    }
    return this.fetch(options);
  };

  Account.prototype.initialize = function() {
    this._auth.on('authenticated', (function(_this) {
      return function() {
        _this.set({
          id: _this._auth.getUser().id
        });
        return _this.fetch();
      };
    })(this));
    return this._auth.on('deauthenticated', (function(_this) {
      return function() {
        return _this.clear();
      };
    })(this));
  };

  Account.getInstance = function() {
    return this.__instance != null ? this.__instance : this.__instance = new this;
  };

  return Account;

})(self.Yungcloud.profile);

self.Yungcloud.TypeAhead = (function(superClass) {
  extend(TypeAhead, superClass);

  function TypeAhead() {
    return TypeAhead.__super__.constructor.apply(this, arguments);
  }

  TypeAhead.prototype.model = Backbone.Model.extend({
    valueOf: function() {
      return {
        name: this.attributes.reference_name,
        path: this.attributes.reference_path
      };
    }
  });

  return TypeAhead;

})(self.Yungcloud.Collection);

self.Yungcloud.SearchResults = (function(superClass) {
  extend(SearchResults, superClass);

  function SearchResults() {
    return SearchResults.__super__.constructor.apply(this, arguments);
  }

  SearchResults.prototype.initialize = function() {
    SearchResults.__super__.initialize.apply(this, arguments);
    return this.model.prototype.className = 'Search/query';
  };

  return SearchResults;

})(ApiHeroUI.search.Collection);

self.Yungcloud.Like = (function(superClass) {
  extend(Like, superClass);

  function Like() {
    return Like.__super__.constructor.apply(this, arguments);
  }

  Like.prototype.validate = function() {};

  Like.prototype.initialize = function() {
    Like.__super__.initialize.apply(this, arguments);
    this.className = 'Tracks/like';
    return this.url = this.url();
  };

  return Like;

})(self.Yungcloud.Model);

self.Yungcloud.Comments = (function(superClass) {
  extend(Comments, superClass);

  function Comments() {
    Comments.__super__.constructor.call(this);
    _.extend(this.model, {
      defaults: {
        item_id: "",
        item_type: "",
        message: ""
      }
    });
  }

  return Comments;

})(self.Yungcloud.Collection);

self.Yungcloud.PlaylistTracks = (function(superClass) {
  extend(PlaylistTracks, superClass);

  function PlaylistTracks() {
    var _extend;
    PlaylistTracks.__super__.constructor.call(this);
    _extend = {
      defaults: {
        track_id: "",
        playlist_id: ""
      }
    };
    this.model = this.model.extend(_extend);
  }

  PlaylistTracks.prototype.associate = function(track, playlist, options) {
    var o, playlistId, trackId;
    if (options == null) {
      options = {};
    }
    if (!(trackId = Util.getID(track))) {
      if (typeof options.error === "function") {
        options.error('invalid trackId');
      }
      return false;
    }
    if (!(playlistId = Util.getID(playlist))) {
      if (typeof options.error === "function") {
        options.error('invalid playlistId');
      }
      return false;
    }
    o = {
      track_id: trackId,
      playlist_id: playlistId
    };
    console.log(this.url());
    return this.create(o, _.extend(options, {
      wait: true,
      validate: false,
      url: this.url()
    }));
  };

  PlaylistTracks.prototype.deassociate = function(track, playlist, options) {
    var m, playlistId, trackId;
    if (options == null) {
      options = {};
    }
    if (!(trackId = Util.getID(track))) {
      if (typeof options.error === "function") {
        options.error('invalid trackId');
      }
      return false;
    }
    if (!(playlistId = Util.getID(playlist))) {
      if (typeof options.error === "function") {
        options.error('invalid playlistId');
      }
      return false;
    }
    if (!(m = this.findWhere({
      track_id: trackId,
      playlist_id: playlistId
    }))) {
      if (typeof options.error === "function") {
        options.error('unable to find playlist and track association');
      }
      return false;
    }
    return m.destroy(_.extend(options, {
      wait: true
    }));
  };

  PlaylistTracks.getInstance = function() {
    if (this.__instance == null) {
      this.__instance = new this;
    }
    this.__instance.className = 'PlaylistTracks';
    return this.__instance;
  };

  return PlaylistTracks;

})(self.Yungcloud.Collection);

self.Yungcloud.Playlists = (function(superClass) {
  extend(Playlists, superClass);

  function Playlists() {
    Playlists.__super__.constructor.call(this);
    this.model.prototype.defaults = {
      name: "",
      description: "",
      "public": "",
      account_id: "",
      creator: {},
      tracks: []
    };
    this.model.prototype.hasTrack = function(trackOrId) {
      var auth, trackId;
      if (!((trackOrId != null) && (trackId = Util.getID(trackOrId)))) {
        return false;
      }
      if (!(auth = Yungcloud.Auth.getInstance()).isAuthenticated()) {
        return false;
      }
      console.log("checking " + trackId);
      return 0 <= (_.pluck(this.attributes.tracks, 'id')).indexOf(trackId);
    };
    this.model.prototype.addTrack = function(trackOrId, options) {
      var _opts, o, trackId;
      if (options == null) {
        options = {};
      }
      if (!(trackId = Util.getID(trackOrId))) {
        if (typeof options.error === "function") {
          options.error('invalid trackId');
        }
        return false;
      }
      _opts = {
        type: 'put',
        validate: false,
        url: (this.url().replace('?', '')) + "/tracks/rel/" + trackId
      };
      o = {
        track_id: trackId,
        playlist_id: this.get('id')
      };
      return this.clone().clear({
        silent: true
      }).save(o, _.extend(options, _opts));
    };
    this.model.prototype.removeTrack = function(trackOrId, options) {
      var _opts, m, trackId;
      if (options == null) {
        options = {};
      }
      if (!(trackId = Util.getID(trackOrId))) {
        if (typeof options.error === "function") {
          options.error('invalid trackId');
        }
        return false;
      }
      _opts = {
        type: 'delete',
        validate: false,
        url: (this.url().replace('?', '')) + "/tracks/rel/" + trackId
      };
      return (m = this.clone()).clear({
        silent: true
      }).sync('delete', m, _.extend(options, _opts));
    };
  }

  return Playlists;

})(self.Yungcloud.Collection);
/*
#(=) require_tree .
 */
;
