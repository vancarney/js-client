RikkiTikki = require( 'riiki-tikki-client' ) if (module?.exports?)
self = exports ? window
self.Yungcloud = RikkiTikki.createScope 'yungcloud'
self.Yungcloud.Object::idAttribute  = 'id'
Yungcloud.Auth::persist             = true
Yungcloud.Login::className          = 'User'
Yungcloud.API_VERSION               = 'v1'

# self.Yungcloud.users::defaults.passwordReset = "#{Yungcloud.API_URI}/reset"
# self.Yungcloud.users::resetPassword = (model, options)->
  # user.unset 'password_confirmation'
  # user.set model
  # user.resetPassword options
class Yungcloud.track extends Yungcloud.Model
  defaults:
    title:null
    buy:null
    account_id:null
    description:null
    tag:null
    size:0
    public:1
    download:0
    license:"all rights reserved"
  # url: ->
    # url = "#{Yungcloud.getAPIUrl()}/accounts/#{@id}"
    # filter = JSON.stringify include: ["covers", "comments", "views", "playlists"]
    # "#{url}?filter=#{filter}"
  # set:->
    # Backbone.Model::set.apply @, arguments
  validate:(o)->
    _.extend @attributes, o
    return "track title required" unless o.title?.length
    return "tags required" unless o.tag?.length
    if o.buy?.length
      unless o.buy.match /^(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/ig
        return "buy link must be a valid URL"
    return
  save:->
    Backbone.Model::save.apply @, arguments
  addPlaylist = (playlistOrId, options={})->
    unless (playlistId = Util.getID playlistOrId)
      options.error? 'invalid playlistId'
      return false
    _opts =
      type: 'put'
      validate: false
      url: "#{@url().replace '?', ''}/playlists/rel/#{playlistId}"
      success:(m,r,o)=>
        @fetch if options.success? then success: options.success else {}
    o = {track_id: @get( 'id' ), playlist_id: playlistId}
    @clone().clear(silent: true).save o, _.extend options, _opts
  removePlaylist = (playlistOrId, options={})->
    unless (playlistId = Util.getID playlistOrId)
      options.error? 'invalid playlistId'
      return false
    _opts =
      url: "#{@url().replace '?', ''}/playlists/rel/#{playlistId}"
      success:(m,r,o)=>
        @fetch if options.success? then success: options.success else {}
    o = {track_id: @get( 'id' ), playlist_id: playlistId}
    @clone().clear(silent: true).destroy _.extend options, _opts  
# class Yungcloud.Buddies extends Yungcloud.Model
  # _followers: new (Yungcloud.Model.extend
    # className:"Followers"
#       
  # follow:(id)->
  # unfollow:(id)->
    
class Yungcloud.Register extends Yungcloud.Model
  className:'Account'#'User'
  defaults:
    username:""
    password:""
    email:""
  # register:->
    # @save
      # username:@get 'username'
      # passowrd:@get 'passowrd'
      # email:@get 'email'
  validate:(o)->
    _.extend @attributes, o
    return "username required" unless o.username?.length
    return "email required" unless o.email?.length
    return "password required" unless o.password?.length
class self.Yungcloud.tracks extends self.Yungcloud.Collection
  model:self.Yungcloud.track
class self.Yungcloud.profile extends self.Yungcloud.Model
  defaults:
    firstName:""
    lastName:""
    email:""
    city:""
    country:""
    postal:""
    image:""
    cover:""
class self.Yungcloud.Settings extends self.Yungcloud.Model
  _auth: Yungcloud.Auth.getInstance()
  defaults:
    account_id: null,
    notify_likes: true,
    notify_comments: true,
    notify_chats: true,
    notify_friends: true,
    email_comments: true,
    email_likes: true,
    email_friends: true
  validate:(o)->
    console.log o
    return
  fetch:->
    return false unless @_auth.isAuthenticated() and @get('account_id')?
    Settings.__super__.fetch.apply @, arguments
  save:(model, options={})->
    return false unless @_auth.isAuthenticated() and @get('account_id')?
    options.type = 'put'
    Settings.__super__.save.call @, model, options
  destroy:->
    false
  initialize: ->
    if @_auth.isAuthenticated()
      @set {account_id: @_auth.getUser().id}, silent: true
    @_auth.on 'authenticated', =>
      @set account_id: @_auth.getUser().id
      @fetch()
    @_auth.on 'deauthenticated', =>
      @clear()
class self.Yungcloud.profiles extends self.Yungcloud.Collection
  model:self.Yungcloud.profile
# class self.Yungcloud.users extends self.Yungcloud.Model
  # _auth: Yungcloud.Auth.getInstance()
  # reset: ()->
    # opts.type = 'post'
    # opts.url = "#{@url().replace '?', ''}/reset"
class self.Yungcloud.Account extends self.Yungcloud.profile
  _auth: Yungcloud.Auth.getInstance()
  sync: ->
    return throw 'cannot operate on Account object without valid user' unless @_auth.isAuthenticated() and @get( 'id' )?
    Account.__super__.sync.apply @, arguments
  resetPassword: (m, opts)->
    (new Yungcloud.users).reset.apply @, arguments
  getSettings:(options)->
    return unless (id = @get 'id')?
    options.url = "#{@url().split('?')[0]}/settings"
    (new Yungcloud.Settings).fetch options
  url: ->
    url = "#{Yungcloud.getAPIUrl()}/accounts/#{@id}"
    filter = JSON.stringify include: ["avatars", "banners"]
    "#{url}?filter=#{filter}"
  updateSettings:(value, options)->
    return unless (id = @get 'id')?
    delete value.id if value.hasOwnProperty 'id'
    options.type = 'put'
    options.url = "#{@url()}/#{id}/settings"
    (new Yungcloud.Settings id: id).save value, options
  refresh:(options={})->
    @fetch options
  initialize: ->
    @_auth.on 'authenticated', =>
      @set id: @_auth.getUser().id
      @fetch()
    @_auth.on 'deauthenticated', =>
      @clear()
  @getInstance: ->
    @__instance ?= new @
class self.Yungcloud.TypeAhead extends self.Yungcloud.Collection
  model:Backbone.Model.extend
    valueOf:->
      name: @attributes.reference_name, path: @attributes.reference_path
class self.Yungcloud.SearchResults extends ApiHeroUI.search.Collection #self.Yungcloud.Collection
  initialize:->
    SearchResults.__super__.initialize.apply @, arguments
    @model::className = 'Search/query'
class self.Yungcloud.Like extends self.Yungcloud.Model
  validate:->
  initialize:->
    Like.__super__.initialize.apply @, arguments
    @className = 'Tracks/like'
    @url = @url()
class self.Yungcloud.Comments extends self.Yungcloud.Collection
  constructor:->
    super()
    _.extend @model, defaults: item_id: "", item_type: "", message: ""
class self.Yungcloud.PlaylistTracks extends self.Yungcloud.Collection
  constructor:->
    super()
    _extend =
      defaults:
        track_id: ""
        playlist_id: "" 
    @model = @model.extend _extend
  associate: (track, playlist, options={})->
    unless (trackId = Util.getID track)
      options.error? 'invalid trackId'
      return false
    unless (playlistId = Util.getID playlist)
      options.error? 'invalid playlistId'
      return false
    o = track_id: trackId, playlist_id: playlistId
    console.log @url()
    @create o, _.extend options, { wait: true, validate: false, url: @url() }
  deassociate: (track, playlist, options={})->
    unless (trackId = Util.getID track)
      options.error? 'invalid trackId'
      return false
    unless (playlistId = Util.getID playlist)
      options.error? 'invalid playlistId'
      return false
    unless (m = @findWhere {track_id: trackId, playlist_id: playlistId})
      options.error? 'unable to find playlist and track association'
      return false 
    m.destroy _.extend options, wait: true
  @getInstance: ->
    @__instance ?= new @
    @__instance.className = 'PlaylistTracks'
    @__instance
class self.Yungcloud.Playlists extends self.Yungcloud.Collection
  constructor:->
    super()
    @model::defaults =
      name: ""
      description: ""
      public: ""
      account_id:""
      creator: {}
      tracks: []
    @model::hasTrack = (trackOrId)->
      return false unless trackOrId? and trackId = Util.getID trackOrId
      return false unless (auth = Yungcloud.Auth.getInstance()).isAuthenticated()
      console.log "checking #{trackId}"
      0 <= (_.pluck @attributes.tracks, 'id').indexOf trackId
    @model::addTrack = (trackOrId, options={})->
      unless (trackId = Util.getID trackOrId)
        options.error? 'invalid trackId'
        return false
      _opts =
        type: 'put'
        validate: false
        url: "#{@url().replace '?', ''}/tracks/rel/#{trackId}"
      o = {track_id: trackId, playlist_id: @get( 'id' )}
      @clone().clear(silent: true).save o, _.extend options, _opts
    @model::removeTrack = (trackOrId, options={})->
      unless (trackId = Util.getID trackOrId)
        options.error? 'invalid trackId'
        return false
      _opts =
        type: 'delete'
        validate: false
        url: "#{@url().replace '?', ''}/tracks/rel/#{trackId}"
      (m = @clone()).clear(silent: true).sync 'delete', m,  _.extend options, _opts   